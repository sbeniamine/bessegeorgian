README.md
Georgian Paralex

0 - Acknowledgements

I wish to express my gratitude towards Sacha Beniamine (https://sacha.beniamine.net/) for his help with the conversion to the Paralex format (https://sbeniamine.gitlab.io/paralex/) as well as his corrections and feedback.

I wish to express my gratitude towards Professor Ioana Chitoran (https://clillac-arp.u-paris.fr/annuaire/chitoran-ioana/) and Professor Olivier Bonami (http://www.llf.cnrs.fr/fr/Gens/Bonami) for their help and insightful advice, and for their involvement in the elaboration of the original database.

1 - Project Overview

The initial objective of this project is to catalog and segment the maximum number of verbal forms in Georgian, to be used for research purposes. This project serves as an open database for the study of Georgian morphology, as well as the morpho-semantic interface of the Georgian language. These forms are extracted from the texts of the Georgian National Corpus (GNC), available here: http://gnc.gov.ge/gnc/.

The GNC offers several corpora, and the majority of the forms come from the Georgian Reference Corpus (> 200 million tokens). Other forms are sourced from the small corpora of modern Georgian (GNC Modern) and political texts (GNC Political). The extracted forms are subsequently processed in Python 3 using the pandas module. The database is then converted to the Paralex format and segmented.